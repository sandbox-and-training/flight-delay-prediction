CREATE TABLE IF NOT EXISTS flights (
    flight_id INT AUTO_INCREMENT PRIMARY KEY,
    flight_date DATE,
    airline VARCHAR(100),
    flight_number VARCHAR(50),
    departure_airport VARCHAR(100),
    arrival_airport VARCHAR(100),
    scheduled_departure_time DATETIME,
    actual_departure_time DATETIME,
    departure_delay INT,
    scheduled_arrival_time DATETIME,
    actual_arrival_time DATETIME,
    arrival_delay INT,
    status VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS optimized_flights (
    flight_id INT AUTO_INCREMENT PRIMARY KEY,
    flight_date DATE,
    airline VARCHAR(100),
    flight_number VARCHAR(50),
    departure_airport VARCHAR(100),
    arrival_airport VARCHAR(100),
    scheduled_departure_time DATETIME,
    actual_departure_time DATETIME,
    departure_delay INT,
    scheduled_arrival_time DATETIME,
    actual_arrival_time DATETIME,
    arrival_delay INT,
    predicted_arrival_delay INT,
    status VARCHAR(50)
);