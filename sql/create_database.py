import os
import logging
from sqlalchemy import create_engine, text
from sqlalchemy.exc import SQLAlchemyError
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Get the database URI from the environment variable
db_uri = os.getenv('DB_URI')

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def create_database():
    try:
        logger.info("Starting the database creation process.")
        
        # Create a SQLAlchemy engine
        engine = create_engine(db_uri)
        logger.info("SQLAlchemy engine created.")

        # Read the SQL script
        sql_file_path = os.path.join(os.path.dirname(__file__), 'create_tables.sql')
        with open(sql_file_path, 'r') as file:
            create_tables_sql = file.read()
        logger.info("SQL script read from file.")

        # Execute the SQL script to create tables
        with engine.connect() as connection:
            for statement in create_tables_sql.split(';'):
                if statement.strip():
                    connection.execute(text(statement))
        logger.info("Database and tables created successfully.")
    except SQLAlchemyError as e:
        logger.error(f"Error creating database: {e}")
    except Exception as e:
        logger.error(f"An unexpected error occurred: {e}")

if __name__ == "__main__":
    create_database()