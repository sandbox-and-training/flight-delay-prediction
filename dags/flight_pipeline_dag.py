from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
}

dag = DAG(
    'flight_pipeline',
    default_args=default_args,
    description='A simple flight data pipeline',
    schedule_interval='@hourly',  # Schedule to run hourly
    start_date=days_ago(1),
    catchup=False,
)

create_db = BashOperator(
    task_id='create_db',
    bash_command='python /opt/airflow/sql/create_database.py',
    dag=dag,
)

data_collection = BashOperator(
    task_id='data_collection',
    bash_command='python /opt/airflow/scripts/data_collection.py',
    dag=dag,
)

data_processing = BashOperator(
    task_id='data_processing',
    bash_command='python /opt/airflow/scripts/data_processing.py',
    dag=dag,
)

load_to_sql = BashOperator(
    task_id='load_to_sql',
    bash_command='python /opt/airflow/scripts/load_to_sql.py',
    dag=dag,
)

train_model = BashOperator(
    task_id='train_model',
    bash_command='python /opt/airflow/scripts/train_model.py',
    dag=dag,
)

optimize_schedule = BashOperator(
    task_id='optimize_schedule',
    bash_command='python /opt/airflow/scripts/optimize_schedule.py',
    dag=dag,
)

visualize_data = BashOperator(
    task_id='visualize_data',
    bash_command='python /opt/airflow/scripts/visualize_data.py',
    dag=dag,
)

create_db >> data_collection >> data_processing >> load_to_sql >> train_model >> optimize_schedule >> visualize_data