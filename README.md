# Flight Delay Prediction

This project aims to predict and optimize flight delays using machine learning models. The project fetches flight data using Apache Spark from the OpenSky Network API, processes it, trains a model to predict delays, and optimizes the flight schedule accordingly. The project is containerized using Docker and includes a pipeline orchestrated with Apache Airflow.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup and Installation](#setup-and-installation)
- [Usage](#usage)
- [Airflow Setup](#airflow-setup)

## Prerequisites

- Docker
- Docker Compose

## Setup and Installation

1. **Clone the repository:**
    ```sh
    git clone https://gitlab.com/sandbox-and-training/flight-delay-prediction
    cd flight-delay-prediction
    ```

2. **Configure environment variables:**
    Create a `.env` file in the root directory and provide the required environment variables:
    ```
    OPENSKY_USERNAME=your_opensky_username
    OPENSKY_PASSWORD=your_opensky_password
    DB_URI=mysql+pymysql://root:password@db/flight_db
    ```

3. **Build and run the Docker containers:**
    ```sh
    docker compose up --build
    ```

## Usage

The main services in this project are defined in `compose.yml` and include:

- **app**: The main application service that runs the entire pipeline: data collection, processing, loading to SQL, model training, and optimization.
- **db**: The MySQL database service.

When the containers are up, the application will automatically start fetching data, processing it, and running the machine learning pipeline.

## Airflow Setup

The project includes Apache Airflow for orchestrating the pipeline. Follow these steps to set up and run Airflow:

1. **Configure Airflow:**
    Update the `config/airflow/airflow.cfg` file with the settings.

2. **Build and run Airflow containers:**
    ```sh
    docker compose -f compose.airflow.yml up --build
    ```

3. **Access Airflow Web UI:**
    Open the browser and go to `http://localhost:8080` to access the Airflow UI. It can manually trigger DAG runs or let them run on a schedule.