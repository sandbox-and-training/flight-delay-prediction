#!/bin/bash
# Wait for PostgreSQL to be ready
while ! nc -z postgres 5432; do
  echo "Waiting for PostgreSQL..."
  sleep 2
done

# Initialize the database
airflow db init

# Start the scheduler and webserver
airflow scheduler & airflow webserver