import json
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, to_date, udf, lit
from pyspark.sql.types import IntegerType, StringType
import os
import logging

# Initialize Spark session
spark = SparkSession.builder.appName("FlightDataProcessing").getOrCreate()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def preprocess_data(input_file='data/raw/flights_data.csv', output_file='data/processed/processed_flights_data.csv'):
    # Read the raw data
    df = spark.read.csv(input_file, header=True, inferSchema=True)

    # Rename columns based on OpenSky API data
    df = df.withColumnRenamed('icao24', 'flight_number') \
           .withColumnRenamed('firstSeen', 'scheduled_departure_time') \
           .withColumnRenamed('lastSeen', 'scheduled_arrival_time') \
           .withColumnRenamed('estDepartureAirport', 'departure_airport') \
           .withColumnRenamed('estArrivalAirport', 'arrival_airport') \
           .withColumnRenamed('callsign', 'airline')

    logger.info("Schema after renaming columns:")
    df.printSchema()

    # Calculate the delays
    df = df.withColumn('departure_delay', (col('scheduled_arrival_time') - col('scheduled_departure_time')).cast(IntegerType()))
    df = df.withColumn('arrival_delay', (col('scheduled_arrival_time') - col('scheduled_departure_time')).cast(IntegerType()))

    # Convert Unix timestamps to readable datetime format
    df = df.withColumn('scheduled_departure_time', col('scheduled_departure_time').cast(IntegerType()))
    df = df.withColumn('scheduled_arrival_time', col('scheduled_arrival_time').cast(IntegerType()))

    # Drop rows with null values in crucial columns
    required_columns = ['departure_delay', 'arrival_delay', 'scheduled_departure_time', 'airline', 'flight_number', 'departure_airport', 'arrival_airport']
    df_cleaned = df.dropna(subset=required_columns)

    # Convert flight_date to date type
    df_cleaned = df_cleaned.withColumn('flight_date', to_date(col('scheduled_departure_time').cast(StringType()), 'yyyy-MM-dd'))

    # Ensure 'status' is string type
    df_cleaned = df_cleaned.withColumn('status', lit('active').cast(StringType()))

    logger.info("Schema before saving:")
    df_cleaned.printSchema()

    # Save the cleaned data to CSV
    df_cleaned.write.csv(output_file, header=True, mode='overwrite')
    logger.info(f"Data preprocessed and saved to {output_file} successfully.")

if __name__ == "__main__":
    preprocess_data()