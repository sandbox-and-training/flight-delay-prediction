from pyspark.sql import SparkSession
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegressionModel
from pyspark.sql.functions import col
from sqlalchemy import create_engine
from dotenv import load_dotenv
import os
import logging

# Initialize Spark session
spark = SparkSession.builder.appName("FlightScheduleOptimization").getOrCreate()

# Load environment variables from .env file
load_dotenv()

# Get the database URI from the environment variable
db_uri = os.getenv('DB_URI')

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Function to optimize flight schedules
def optimize_schedule(input_file='data/processed/processed_flights_data.csv', model_path='models/flight_delay_model', output_file='data/optimized/optimized_schedule.csv'):
    try:
        # Load the processed data
        df = spark.read.csv(f"{input_file}/*", header=True, inferSchema=True)
        logger.info(f"Loaded processed data with schema: {df.schema}")

        # Load the trained model
        lr_model = LinearRegressionModel.load(model_path)
        logger.info(f"Loaded trained model from {model_path}")

        # Prepare the data for prediction
        feature_columns = ['departure_delay']  # Adjust based on the features used during training
        assembler = VectorAssembler(inputCols=feature_columns, outputCol='features')
        df = assembler.transform(df)
        logger.info("Data transformed using VectorAssembler")

        # Predict delays
        predictions = lr_model.transform(df)
        logger.info("Predictions made on the data")

        # Assuming the optimization involves minimizing delays
        optimized_df = predictions.withColumn('predicted_arrival_delay', col('prediction'))
        logger.info("Predicted arrival delays added to DataFrame")

        # Ensure the output directory exists
        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        logger.info(f"Output directory {os.path.dirname(output_file)} created")

        # Save the optimized schedule
        optimized_df.select('flight_date', 'status', 'departure_delay', 'arrival_delay', 'airline', 'flight_number', 'departure_airport', 'arrival_airport', 'predicted_arrival_delay') \
                    .toPandas().to_csv(output_file, index=False)
        logger.info(f"Optimized schedule saved to {output_file}")

        # Load optimized schedule into SQL database
        engine = create_engine(db_uri)
        optimized_df.select('flight_date', 'status', 'departure_delay', 'arrival_delay', 'airline', 'flight_number', 'departure_airport', 'arrival_airport', 'predicted_arrival_delay') \
                    .toPandas().to_sql('optimized_flights', engine, if_exists='replace', index=False)
        logger.info("Optimized schedule loaded into the SQL database successfully")
    except Exception as e:
        logger.error(f"An error occurred while optimizing the schedule: {e}")
        raise

if __name__ == "__main__":
    optimize_schedule()