from pyspark.sql import SparkSession
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression
from pyspark.ml.evaluation import RegressionEvaluator
import logging

# Initialize Spark session
spark = SparkSession.builder.appName("FlightDelayModelTraining").getOrCreate()

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def train_model(input_file='data/processed/processed_flights_data.csv', model_path='models/flight_delay_model'):
    try:
        # Read the processed data
        df = spark.read.csv(input_file, header=True, inferSchema=True)
        logger.info(f"Loaded processed data with schema: {df.schema}")

        # Print schema before transformation
        logger.info("Schema before VectorAssembler transformation:")
        df.printSchema()

        # Prepare the data for training
        feature_columns = ['departure_delay']  # Features used for training
        assembler = VectorAssembler(inputCols=feature_columns, outputCol='features')
        df = assembler.transform(df)
        logger.info("Data transformed using VectorAssembler")

        # Print schema after transformation
        logger.info("Schema after VectorAssembler transformation:")
        df.printSchema()

        # Select the features and label
        df = df.select('features', 'arrival_delay')

        # Split the data into training and test sets
        train_data, test_data = df.randomSplit([0.8, 0.2], seed=1234)
        logger.info(f"Data split into training and test sets: {train_data.count()} training rows, {test_data.count()} test rows")

        # Check if training data is empty
        if train_data.count() == 0:
            raise ValueError("Training dataset is empty. Ensure you have enough historical data.")

        # Initialize the Linear Regression model
        lr = LinearRegression(featuresCol='features', labelCol='arrival_delay')

        # Train the model
        lr_model = lr.fit(train_data)
        logger.info("Model training completed")

        # Overwrite the existing model
        lr_model.write().overwrite().save(model_path)
        logger.info(f"Model saved at {model_path}")

        # Evaluate the model on test data
        predictions = lr_model.transform(test_data)
        evaluator = RegressionEvaluator(labelCol='arrival_delay', predictionCol='prediction', metricName='rmse')
        rmse = evaluator.evaluate(predictions)
        logger.info(f"Root Mean Squared Error (RMSE) on test data: {rmse}")
    except Exception as e:
        logger.error(f"An error occurred during model training: {e}")
        raise

if __name__ == "__main__":
    train_model()