import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sqlalchemy import create_engine
from dotenv import load_dotenv
import os
import logging

# Load environment variables from .env file
load_dotenv()

# Get the database URI from the environment variable
db_uri = os.getenv('DB_URI')

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def load_data(table_name):
    """
    Load data from the specified SQL table into a pandas DataFrame.
    """
    try:
        # Create a SQLAlchemy engine
        engine = create_engine(db_uri)

        # Load data from the SQL database
        df = pd.read_sql_table(table_name, engine)
        logger.info(f"Data loaded from table {table_name} with {len(df)} records.")
        return df
    except Exception as e:
        logger.error(f"Failed to load data from table {table_name}: {e}")
        raise

def plot_delay_distribution(df, column, title, filename):
    """
    Plot the distribution of delays.
    """
    try:
        plt.figure(figsize=(10, 6))
        sns.histplot(df[column], bins=50, kde=True)
        plt.title(title)
        plt.xlabel('Delay (minutes)')
        plt.ylabel('Frequency')
        plt.xticks(rotation=45, fontsize=10)
        plt.grid(True, which='both', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        
        # Ensure the output directory exists
        os.makedirs('visualizations', exist_ok=True)

        plt.savefig(f'visualizations/{filename}')
        plt.close()
        logger.info(f"Plot saved as visualizations/{filename}")
    except Exception as e:
        logger.error(f"Failed to create delay distribution plot for {column}: {e}")
        raise

def plot_scatter(df, x_col, y_col, title, filename):
    """
    Plot a scatter plot of delays.
    """
    try:
        plt.figure(figsize=(10, 6))
        sns.scatterplot(data=df, x=x_col, y=y_col)
        plt.title(title)
        plt.xlabel(x_col.replace('_', ' ').title())
        plt.ylabel(y_col.replace('_', ' ').title())
        plt.xticks(rotation=45, fontsize=10)
        plt.grid(True, which='both', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        
        # Ensure the output directory exists
        os.makedirs('visualizations', exist_ok=True)

        plt.savefig(f'visualizations/{filename}')
        plt.close()
        logger.info(f"Scatter plot saved as visualizations/{filename}")
    except Exception as e:
        logger.error(f"Failed to create scatter plot for {x_col} vs {y_col}: {e}")
        raise

def visualize_data():
    """
    Load the data from the SQL tables and generate the visualizations.
    """
    try:
        # Load the processed data
        processed_df = load_data('flights')

        # Plot distribution of departure delays
        plot_delay_distribution(processed_df, 'departure_delay', 'Distribution of Departure Delays', 'departure_delay_distribution.png')

        # Plot distribution of arrival delays
        plot_delay_distribution(processed_df, 'arrival_delay', 'Distribution of Arrival Delays', 'arrival_delay_distribution.png')

        # Scatter plot of departure vs arrival delays
        plot_scatter(processed_df, 'departure_delay', 'arrival_delay', 'Departure Delay vs Arrival Delay', 'departure_vs_arrival_delay.png')

        # Load the optimized data
        optimized_df = load_data('optimized_flights')

        # Scatter plot of predicted vs actual arrival delays
        plot_scatter(optimized_df, 'predicted_arrival_delay', 'arrival_delay', 'Predicted vs Actual Arrival Delays', 'predicted_vs_actual_arrival_delay.png')
    except Exception as e:
        logger.error(f"An error occurred during data visualization: {e}")
        raise

if __name__ == "__main__":
    visualize_data()