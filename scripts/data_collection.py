import requests
import pandas as pd
from dotenv import load_dotenv
import os
from datetime import datetime, timedelta, timezone
import time
import logging

load_dotenv()  # Load environment variables from .env file

USERNAME = os.getenv('OPENSKY_USERNAME')
PASSWORD = os.getenv('OPENSKY_PASSWORD')
BASE_URL = 'https://opensky-network.org/api'

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def fetch_flight_data(start_time, end_time):
    url = f'{BASE_URL}/flights/all?begin={start_time}&end={end_time}'
    logger.info(f"Fetching data from URL: {url}")
    response = requests.get(url, auth=(USERNAME, PASSWORD))
    if response.status_code != 200:
        logger.error(f"Error response: {response.content}")
    response.raise_for_status()
    data = response.json()
    return data

def save_to_csv(flights, filename='data/raw/flights_data.csv'):
    if not flights:
        logger.info("No flights data to save.")
        return
    df = pd.DataFrame(flights)
    if not os.path.exists(filename):
        df.to_csv(filename, index=False)
    else:
        df.to_csv(filename, mode='a', header=False, index=False)
    logger.info(f"Data fetched and saved to {filename} successfully.")

if __name__ == "__main__":
    all_flights = []

    current_time = datetime.now(timezone.utc)
    end_time = int(current_time.timestamp())
    start_time = end_time - 3600  # 1 hour ago

    interval = 300  # 5 minutes in seconds
    for t in range(start_time, end_time, interval):
        try:
            flights = fetch_flight_data(t, t + interval)
            if flights:
                all_flights.extend(flights)
            else:
                logger.info(f"No flights data found for interval {t} to {t + interval}")
            time.sleep(1)  # sleep for 1 second between requests
        except requests.exceptions.HTTPError as e:
            logger.error(f"HTTPError for interval {t} to {t + interval}: {e}")

    save_to_csv(all_flights)