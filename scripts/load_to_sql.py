import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
from dotenv import load_dotenv
import os
import logging

# Load environment variables from .env file
load_dotenv()

# Get the database URI from the environment variable
db_uri = os.getenv('DB_URI')

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def load_to_sql(input_dir='data/processed/processed_flights_data.csv', table_name='flights'):
    try:
        # Create a SQLAlchemy engine
        engine = create_engine(db_uri)
        logger.info(f"Created SQLAlchemy engine with URI: {db_uri}")

        # Check if input_dir is a directory or a single file
        if os.path.isdir(input_dir):
            # List all files in the input directory and filter for part files
            csv_files = [os.path.join(input_dir, file) for file in os.listdir(input_dir) if file.startswith('part-') and file.endswith('.csv')]
        else:
            csv_files = [input_dir]  # Treat as a single file

        # Print the list of detected files
        logger.info(f"CSV files detected for concatenation: {csv_files}")

        # Check if there are any files to concatenate
        if not csv_files:
            logger.error("No CSV files found to concatenate in the specified directory.")
            raise ValueError("No CSV files found to concatenate in the specified directory.")

        # Read all part files into a single DataFrame
        df = pd.concat((pd.read_csv(f) for f in csv_files))
        logger.info(f"Loaded data into DataFrame with shape: {df.shape}")

        # Load existing data from the database
        try:
            existing_df = pd.read_sql_table(table_name, engine)
            logger.info(f"Loaded existing data from {table_name} table with shape: {existing_df.shape}")
        except SQLAlchemyError as e:
            logger.warning(f"Error reading existing data: {e}")
            existing_df = pd.DataFrame()

        # Append new data, avoiding duplicates
        combined_df = pd.concat([existing_df, df]).drop_duplicates(subset=['flight_number', 'scheduled_departure_time'])
        logger.info(f"Combined DataFrame shape after removing duplicates: {combined_df.shape}")

        # Load data into the SQL database using the database URI string
        combined_df.to_sql(table_name, con=engine, if_exists='replace', index=False)
        logger.info(f"Data loaded successfully into {table_name} table.")
    except Exception as e:
        logger.error(f"An error occurred while loading data to SQL: {e}")
        raise

if __name__ == "__main__":
    load_to_sql()